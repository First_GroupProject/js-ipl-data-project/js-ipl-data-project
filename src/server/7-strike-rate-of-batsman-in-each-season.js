const fs = require("fs");

const path = require("path");
const convertCsvToJson = require("../../index.js");

const csvFilePathMatches = path.join(__dirname, "../data/matches.csv");
const csvFilePathDeliveries = path.join(__dirname, "../data/deliveries.csv");
const outputFilePath = path.join( __dirname, "../public/output/strikeRateOfBatsmanInEachSeason.json");

function strikeRateOfBatsmanInEachSeason() {
  let matchesData = {};
  convertCsvToJson(csvFilePathMatches)
    .then((matches) => {
      for (let index = 0; index < matches.length; index++) {
        let year = matches[index].season;
        let matchId = matches[index].id;
        matchesData[matchId] = year;
      }
    })
    .catch((error) => {
      console.error(error);
    });

  let batsmanInformation = {};
  convertCsvToJson(csvFilePathDeliveries).then((deliveries) => {
    for (let index = 0; index < deliveries.length; index++) {
      let match_id = deliveries[index].match_id;
      let season = matchesData[match_id];
      let batsman = deliveries[index].batsman;
      let total_runs = deliveries[index].total_runs;

      if (batsmanInformation[batsman]) {
        if (batsmanInformation[batsman][season]) {
          batsmanInformation[batsman][season].total_runs +=
            parseInt(total_runs);
          batsmanInformation[batsman][season].totalBalls += 1;
        } else {
          batsmanInformation[batsman][season] = {};
          batsmanInformation[batsman][season].total_runs = parseInt(total_runs);
          batsmanInformation[batsman][season].totalBalls = 1;
        }
      } else {
        batsmanInformation[batsman] = {};
        batsmanInformation[batsman][season] = {};
        batsmanInformation[batsman][season].total_runs = parseInt(total_runs);
        batsmanInformation[batsman][season].totalBalls = 1;
      }
    }

    let result = {};

    for (let batsman in batsmanInformation) {
      result[batsman] = {};

      for (let year in batsmanInformation[batsman]) {
        let totalRuns = batsmanInformation[batsman][year].total_runs;
        let totalBalls = batsmanInformation[batsman][year].totalBalls;
        let strikeRate = (totalRuns / totalBalls) * 100;
        result[batsman][year] = strikeRate.toFixed(2);
      }
    }

    fs.writeFile(outputFilePath, JSON.stringify(result, null, 2), (error) => {
      if (error) {
        console.error(error);
      } else {
        console.log("Data saved to output file sucessfully");
      }
    });
  });
}

strikeRateOfBatsmanInEachSeason();
