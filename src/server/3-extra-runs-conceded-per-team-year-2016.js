const fs = require("fs");

const path = require("path");
const convertCsvToJson = require("../../index.js");

const csvFilePathMatches = path.join(__dirname, "../data/matches.csv");
const csvFilePathDeliveries = path.join(__dirname, "../data/deliveries.csv");

const outputFilePath = path.join(__dirname,"../public/output/extraRunsConcededPerTeamYear2016.json");

function extraRunsConcededPerTeamYear2016() {
  let matchesOfSeason = {};
  convertCsvToJson(csvFilePathMatches)
    .then((matches) => {
      for (let index = 0; index < matches.length; index++) {
        let year = matches[index].season;
        let matchId = matches[index].id;

        if (year === "2016") {
          matchesOfSeason[matchId] = year;
        }
      }
      return matchesOfSeason;
    })
    .catch((error) => {
      console.error(error);
    });
  convertCsvToJson(csvFilePathDeliveries)
    .then((deliveries) => {
      let extraRunsByTeams = {};

      for (const delivery of deliveries) {
        if (matchesOfSeason.hasOwnProperty(delivery.match_id)) {
          const team = delivery.bowling_team;
          const extraRuns = Number(delivery.extra_runs);
          if (extraRunsByTeams[team]) {
            extraRunsByTeams[team] += extraRuns;
          } else {
            extraRunsByTeams[team] = extraRuns;
          }
        }
      }

      fs.writeFile(
        outputFilePath,
        JSON.stringify(extraRunsByTeams, null, 2),
        (error) => {
          if (error) {
            console.error(error);
          } else {
            console.log("Data saved to output file sucessfully");
          }
        }
      );
    })
    .catch((error) => {
      console.log(error);
    });
}

extraRunsConcededPerTeamYear2016();

