const fs = require("fs");

const path = require("path");
const convertCsvToJson = require("../../index.js");

const csvFilePathMatches = path.join(__dirname, "../data/matches.csv");
const outputFilePath = path.join(__dirname,"../public/output/matchesWonPerTeamPerYear.json");

function matchesWonPerTeamPerYear() {
  convertCsvToJson(csvFilePathMatches)
    .then((matches) => {
      let result = {};
      for (let index = 0; index < matches.length; index++) {
        let winnerTeam = matches[index].winner;
        let year = matches[index].season;

        if (result[year]) {
          if (result[year][winnerTeam]) {
            result[year][winnerTeam] += 1;
          } else {
            result[year][winnerTeam] = 1;
          }
        } else {
          result[year] = {};
          result[year][winnerTeam] = 1;
        }
      }

      fs.writeFile(outputFilePath, JSON.stringify(result, null, 2), (error) => {
        if (error) {
          console.error(error);
        } else {
          console.log("Data saved to output file sucessfully");
        }
      });
    })
    .catch((error) => {
      console.error(error);
    });
}

matchesWonPerTeamPerYear();
