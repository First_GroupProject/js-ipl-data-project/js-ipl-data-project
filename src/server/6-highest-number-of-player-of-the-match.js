const fs = require("fs");

const path = require("path");
const convertCsvToJson = require("../../index.js");

const csvFilePathMatches = path.join(__dirname, "../data/matches.csv");

const outputFilePath = path.join(
  __dirname,
  "../public/output/highestNumberOfPlayerOfTheMatch.json"
);

function highestNumberOfPlayerOfTheMatch() {
  convertCsvToJson(csvFilePathMatches)
    .then((matches) => {
      let playerOfTheMatch = {};

      for (let index = 0; index < matches.length; index++) {
        let year = matches[index].season;
        let player = matches[index].player_of_match;

        if (playerOfTheMatch[year]) {
          if (playerOfTheMatch[year][player]) {
            playerOfTheMatch[year][player] += 1;
          } else {
            playerOfTheMatch[year][player] = 1;
          }
        } else {
          playerOfTheMatch[year] = {};
          playerOfTheMatch[year][player] = 1;
        }
      }

      let result = {};
      for (let year in playerOfTheMatch) {
        let highestPlayer = "";
        let maximumCount = 0;

        for (let player in playerOfTheMatch[year]) {
          if (playerOfTheMatch[year][player] > maximumCount) {
            highestPlayer = player;
            maximumCount = playerOfTheMatch[year][player];
          }
          result[year] = { [highestPlayer]: maximumCount };
        }
      }

      fs.writeFile(outputFilePath, JSON.stringify(result, null, 2), (error) => {
        if (error) {
          console.error(error);
        } else {
          console.log("Data saved to output file successfully");
        }
      });
    })
    .catch((error) => {
      console.error(error);
    });
}

highestNumberOfPlayerOfTheMatch();
