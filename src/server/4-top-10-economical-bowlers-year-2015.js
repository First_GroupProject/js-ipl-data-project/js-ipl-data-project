const fs = require("fs");

const path = require("path");
const convertCsvToJson = require("../../index.js");

const csvFilePathMatches = path.join(__dirname, "../data/matches.csv");
const csvFilePathDeliveries = path.join(__dirname, "../data/deliveries.csv");

const outputFilePath = path.join(__dirname,"../public/output/top10EconomicalBowlersYear2015.json");

function top10EconomicalBowlers() {
  let matchesOfSeason = {};
  convertCsvToJson(csvFilePathMatches)
    .then((matches) => {
      for (let index = 0; index < matches.length; index++) {
        let year = matches[index].season;
        let matchId = matches[index].id;

        if (year === "2015") {
          matchesOfSeason[matchId] = year;
        }
      }
      return matchesOfSeason;
    })
    .catch((error) => {
      console.error(error);
    });

  convertCsvToJson(csvFilePathDeliveries)
    .then((deliveries) => {
      const runsConceded = {};
      for (let index= 0; index < deliveries.length; index++) {
        const delivery = deliveries[index];
        if (
          matchesOfSeason.hasOwnProperty(delivery.match_id) &&
          delivery.wide_runs == 0 &&
          delivery.noball_runs == 0
        ) {
          if (runsConceded.hasOwnProperty(delivery.bowler)) {
            runsConceded[delivery.bowler].runs += parseInt(delivery.total_runs);
            runsConceded[delivery.bowler].balls += 1;
            runsConceded[delivery.bowler].economy =
              runsConceded[delivery.bowler].runs /
              (runsConceded[delivery.bowler].balls / 6);
          } else {
            runsConceded[delivery.bowler] = {};
            runsConceded[delivery.bowler].runs = parseInt(delivery.total_runs);
            runsConceded[delivery.bowler].balls = 1;
            runsConceded[delivery.bowler].economy =
              runsConceded[delivery.bowler].runs /
              (runsConceded[delivery.bowler].balls / 6);
          }
        }
      }
      
      const bowlersArray = [];
      for (let bowler in runsConceded) {
        bowlersArray.push([bowler, runsConceded[bowler]]);
      }
      for (let index = 0; index < bowlersArray.length - 1; index++) {
        for (let currentIndex = 0; currentIndex < bowlersArray.length - index - 1; currentIndex++) {
          if (bowlersArray[currentIndex][1].economy > bowlersArray[currentIndex + 1][1].economy) {
            const temp = bowlersArray[currentIndex];
            bowlersArray[currentIndex] = bowlersArray[currentIndex + 1];
            bowlersArray[currentIndex + 1] = temp;
          }
        }
      }
      
      const top10EconomicBowlers = [];
      for (let index = 0; index < Math.min(10, bowlersArray.length); index++) {
        top10EconomicBowlers.push(bowlersArray[index][0]);
      }
    
      fs.writeFile(
        outputFilePath,
        JSON.stringify(top10EconomicBowlers, null, 2),
        (error) => {
          if (error) {
            console.error(error);
          } else {
            console.log("Data saved to output file sucessfully");
          }
        }
      );
    })
    .catch((error) => {
      console.error(error);
    });
}

top10EconomicalBowlers();
