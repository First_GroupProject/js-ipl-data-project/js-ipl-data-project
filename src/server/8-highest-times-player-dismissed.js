const fs = require("fs");

const path = require("path");
const convertCsvToJson = require("../../index.js");

const csvFilePathDeliveries = path.join(__dirname, "../data/deliveries.csv");
const outputFilePath = path.join(
  __dirname,
  "../public/output/highestTimesPlayerDismissed.json"
);

function highestTimesPlayerDismissed() {
  convertCsvToJson(csvFilePathDeliveries)
    .then((deliveries) => {
      let result = {};

      for (let index = 0; index < deliveries.length; index++) {
        let currentValue = deliveries[index];
        let batsman = currentValue.batsman;
        let bowler = currentValue.bowler;
        if (currentValue.player_dismissed) {
          if (result[batsman]) {
            if (result[batsman][bowler]) {
              result[batsman][bowler] += 1;
            } else {
              result[batsman][bowler] = 1;
            }
          } else {
            result[batsman] = {};
            result[batsman][bowler] = 1;
          }
        }
      }

      let minimum = 0;
      let batsmanDismissedByBowler = {};

      for (let batsman in result) {
        let maxDismissals = 0;
        let bowlersWithMaxDismissals = [];

        for (let bowler in result[batsman]) {
          const dismissalCount = result[batsman][bowler];
          if (dismissalCount > maxDismissals) {
            maxDismissals = dismissalCount;
            bowlersWithMaxDismissals = [bowler];
          } else if (dismissalCount === maxDismissals) {
            bowlersWithMaxDismissals.push(bowler);
          }
        }

        if (maxDismissals > minimum) {
          minimum = maxDismissals;
          batsmanDismissedByBowler = {
            [batsman]: {
              bowlers: bowlersWithMaxDismissals,
              maxDismissals: maxDismissals,
            },
          };
        }
      }

      fs.writeFile(
        outputFilePath,
        JSON.stringify(batsmanDismissedByBowler, null, 2),
        (error) => {
          if (error) {
            console.error(error);
          } else {
            console.log("Data saved to output file sucessfully");
          }
        }
      );
    })
    .catch((error) => {
      console.error(error);
    });
}

highestTimesPlayerDismissed();
