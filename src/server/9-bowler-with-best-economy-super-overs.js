const fs = require("fs");

const path = require("path");
const convertCsvToJson = require("../../index.js");

const csvFilePathDeliveries = path.join(__dirname, "../data/deliveries.csv");
const outputFilePath = path.join(
  __dirname,
  "../public/output/bowlerWithBestEconomy.json"
);

function bowlersWithBestEconomySuperOvers() {
  convertCsvToJson(csvFilePathDeliveries)
    .then((deliveries) => {
      const bestEconomySuperOver = {};

      for (let i = 0; i < deliveries.length; i++) {
        const current = deliveries[i];
        if (current.is_super_over !== "0") {
          if (bestEconomySuperOver[current.bowler]) {
            bestEconomySuperOver[current.bowler].runs += Number(
              current.total_runs
            );
            bestEconomySuperOver[current.bowler].balls += 1;
            bestEconomySuperOver[current.bowler].economic = (
              bestEconomySuperOver[current.bowler].runs /
              (bestEconomySuperOver[current.bowler].balls / 6)
            ).toFixed(2);
          } else {
            bestEconomySuperOver[current.bowler] = {};
            bestEconomySuperOver[current.bowler].runs = Number(
              current.total_runs
            );
            bestEconomySuperOver[current.bowler].balls = 1;
          }
        }
      }

      let bestEconomy = Infinity;
      let bestBowler = "";
      for (let bowler in bestEconomySuperOver) {
        const economy = parseFloat(bestEconomySuperOver[bowler].economic);
        if (economy < bestEconomy) {
          bestEconomy = economy;
          bestBowler = bowler;
        }
      }
    
      const result = {};
      result[bestBowler] = bestEconomy.toFixed(2);

      fs.writeFile(outputFilePath, JSON.stringify(result, null, 2), (error) => {
        if (error) {
          console.error(error);
        } else {
          console.log("Data saved to output file sucessfully");
        }
      });
    })
    .catch((error) => {
      console.error(error);
    });
}

bowlersWithBestEconomySuperOvers();


