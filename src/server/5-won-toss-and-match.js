const fs = require("fs");

const path = require("path");
const convertCsvToJson = require("../../index.js");

const csvFilePathMatches = path.join(__dirname, "../data/matches.csv");

const outputFilePath = path.join(__dirname,"../public/output/wonTossAndMatch.json");

function numberOfTimesWonTossAndMatch() {
  convertCsvToJson(csvFilePathMatches)
    .then((matches) => {
      let result = {};

      for (let index = 0; index < matches.length; index++) {
        let tossWinner = matches[index].toss_winner;
        let winner = matches[index].winner;
        if (tossWinner === winner) {
          if (result[winner]) {
            result[winner] += 1;
          } else {
            result[winner] = 1;
          }
        }
      }

      fs.writeFile(outputFilePath, JSON.stringify(result, null, 2), (error) => {
        if (error) {
          console.error(error);
        } else {
          console.log("Data saved to output file sucessfully");
        }
      });
    })
    .catch((error) => {
      console.error(error);
    });
}

numberOfTimesWonTossAndMatch();
