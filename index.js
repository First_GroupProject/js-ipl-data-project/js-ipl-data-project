const csv = require("csvtojson");

function convertCsvToJson(csvFilePath) {
  try {
    const jsonArray = csv().fromFile(csvFilePath);
    return jsonArray;
  } catch (error) {
    console.error(error);
  }
}

module.exports = convertCsvToJson;
